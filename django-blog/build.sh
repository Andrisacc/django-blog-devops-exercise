#!/bin/bash
#set -e

SRC_DIR=$(mktemp -d)
git clone https://github.com/mdn/django-diy-blog.git $SRC_DIR/django-diy-blog
cp Dockerfile $SRC_DIR
docker build $SRC_DIR -t $REGISTRY_URL
docker login -u AWS -p $REGISTRY_PASSWD $REGISTRY_URL
docker push $REGISTRY_URL


