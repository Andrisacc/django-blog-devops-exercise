resource "aws_ecr_repository" "ecr_repository" {
  count                = var.create_repository != "" ? 1 : 0
  name                 = var.repository_name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

output "repository_url" {
  value = aws_ecr_repository.ecr_repository[0].repository_url
}
