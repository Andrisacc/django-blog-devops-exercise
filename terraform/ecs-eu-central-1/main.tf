module "ecs_vpc" {
  source         = "../modules/aws/vpc"
  vpc_name       = var.cluster_name
  region_name    = var.region_name
  vpc_cidr_block = var.vpc_cidr_block
}

module "ecs_cluster" {
  source         = "../modules/aws/ecs"
  region_name    = var.region_name
  cluster_name   = var.cluster_name
  instance_type  = var.instance_type
  instance_count = var.instance_count
  vpc_id         = module.ecs_vpc.vpc.id
  subnet_ids     = module.ecs_vpc.subnet_ids
}

