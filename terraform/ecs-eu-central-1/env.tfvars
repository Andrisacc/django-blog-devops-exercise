region_name       = "eu-central-1"
cluster_name      = "ecs-dev-euc1"
instance_type     = "t3.micro"
instance_count    = 3
vpc_cidr_block    = "172.18.0.0/16"
create_repository = true
repository_name   = "django-blog"

