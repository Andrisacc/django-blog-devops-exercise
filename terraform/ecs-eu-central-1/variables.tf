variable "region_name" {}
variable "cluster_name" {}
variable "instance_type" {}
variable "instance_count" {}
variable "vpc_cidr_block" {}
variable "repository_name" {
  default = ""
}
variable "create_repository" {
  default = false
}


