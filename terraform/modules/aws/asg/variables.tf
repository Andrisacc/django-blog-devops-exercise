variable "aws_ami_id" {}
variable "name" {}
variable "function_name" {}
variable "vm_size" {}
variable "environment_name" {}
variable "security_groups" {}
variable "subnet_ids" {}
variable "desired_capacity" {
  default = 1
}
variable "min_size" {
  default = 1
}
variable "max_size" {
  default = 1
}
variable "availability_zones" {
  default = []
}
variable "user_data_base64" {
  default = ""
}
variable "ssh_key_name" {
  default = ""
}

variable "iam_instance_profile" {
  default = ""
}

variable "tags" {
  default = {}
}
variable "associate_public_ip_address" {
  default = true
}
variable "root_volume_size" {
  default = 20
}
