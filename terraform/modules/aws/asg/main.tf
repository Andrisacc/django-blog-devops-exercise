module "tags" {
  source           = "../../tagging"
  name             = var.name
  environment_name = var.environment_name
  tags             = var.tags
}

resource "aws_launch_template" "ec2_amazon2" {
  name_prefix   = var.name
  image_id      = var.aws_ami_id
  instance_type = var.vm_size
  key_name      = var.ssh_key_name
  user_data     = var.user_data_base64

  tag_specifications {
    resource_type = "instance"
    tags          = module.tags.shared_tags
  }

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  network_interfaces {
    security_groups             = var.security_groups
    associate_public_ip_address = var.associate_public_ip_address
  }

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size = var.root_volume_size
      volume_type = "gp2"
    }
  }
}

resource "aws_autoscaling_group" "app_server" {
  name                = var.name
  desired_capacity    = var.desired_capacity
  max_size            = var.max_size
  min_size            = var.min_size
  vpc_zone_identifier = var.subnet_ids

  launch_template {
    id      = aws_launch_template.ec2_amazon2.id
    version = "$Latest"
  }
}

