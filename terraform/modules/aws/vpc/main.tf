data "aws_availability_zones" "region" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }

  filter {
    name   = "region-name"
    values = [var.region_name]
  }
}

locals {
  vpc_subnets = {
    for i, role in data.aws_availability_zones.region.names :
    role => i
  }
}

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "vpc_subnets" {
  for_each = local.vpc_subnets

  vpc_id                  = aws_vpc.main.id
  availability_zone       = each.key
  cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 8, each.value)
  map_public_ip_on_launch = true

  tags = {
    "kubernetes.io/role/elb" = "1"
  }
}

resource "aws_route_table" "public" {
  vpc_id     = aws_vpc.main.id
  depends_on = [aws_internet_gateway.internet_gw]

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_route" "allow_all_in" {
  depends_on             = [aws_route_table.public]
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gw.id

}

resource "aws_route_table_association" "public" {
  for_each       = aws_subnet.vpc_subnets
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id
}

