output "vpc" {
  value = aws_vpc.main
}
output "subnet_ids" {
  value = toset([
    for s in aws_subnet.vpc_subnets : s.id
  ])
}
