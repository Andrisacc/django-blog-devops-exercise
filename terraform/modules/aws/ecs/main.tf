resource "aws_ecs_cluster" "ecs" {
  name = var.cluster_name
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

module "ecs_capacity_provider" {
  source               = "../asg"
  aws_ami_id           = data.aws_ami.amazon_linux_2.id
  name                 = var.cluster_name
  environment_name     = var.cluster_name
  function_name        = "ecs_capacity_provider"
  vm_size              = var.instance_type
  subnet_ids           = var.subnet_ids
  min_size             = var.instance_count
  max_size             = var.instance_count
  desired_capacity     = var.instance_count
  security_groups      = [aws_security_group.ecs_security_group.id]
  user_data_base64     = data.template_cloudinit_config.asg_instance.rendered
  iam_instance_profile = aws_iam_instance_profile.asg_instance.name
}

data "template_file" "instance_userdata" {
  template = file("${path.module}/userdata.sh.tpl")
  vars = {
    cluster_name = var.cluster_name
  }
}

data "template_cloudinit_config" "asg_instance" {
  gzip          = false
  base64_encode = true

  part {
    filename     = "consul_server_userdata.cfg"
    content_type = "text/x-shellscript"
    content      = data.template_file.instance_userdata.rendered
  }
}

resource "aws_security_group" "ecs_security_group" {

  name        = var.cluster_name
  description = var.cluster_name
  vpc_id      = var.vpc_id

  ingress = [
    {
      description      = "Ingress from anywhere"
      from_port        = 0
      to_port          = 65535
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = null
    },

  ]

  egress = [
    {
      description      = "Egress to anywhere"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = null
    }
  ]

  tags = {
    Name = var.cluster_name
  }
}

