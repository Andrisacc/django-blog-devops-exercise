variable "region_name" {}
variable "cluster_name" {}
variable "instance_type" {}
variable "instance_count" {}
variable "vpc_id" {}
variable "subnet_ids" {}

