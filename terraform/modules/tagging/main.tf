output "shared_tags" {
  value = merge({
    "Name"        = var.name
    "start_date"  = "${timestamp()}"
    "environment" = var.environment_name
  }, var.tags)
}
