variable "tags" {
  default = {}
}

variable "environment_name" {
  default = ""
}

variable "name" {
  default = ""
}
