# django-blog-devops-exercise



## Abstract

This repository contais Terraform scripts to provision AWS ECS and ECR resources and as well as script to deploy a service ECS cluster.

Source code of the Python blog that was selected a sample is available Here: https://github.com/mdn/django-diy-blog

## Solution

### Terraform

Inside `terraform` folder you will find all modules and configurations necessary to provision required resources. I decided to describe an ECS cluster with EC2 based capacity provider. EC2 instances are managed within the auto scaling group and are allocated inside deicated VPC. Network access to the nodes is controlled by security group. ECS configuration is passed to the nodes through template rendered userdata script.

I was sticking to modular approach while definig the desired infrastructure state. I have defined several modules: 

- terraform/modules/aws/vpc - to describe VPC and it's subnets
- terraform/modules/aws/asg - to describe auto scaling group and instance launch template
- terraform/modules/aws/ecs - to describe ECS cluster resource and initialize capacity provider
- terraform/modules/tags - an auxilary module for rendering common tags for resources 

These modules makes ECS cluster definition as simple as following: 
```terraform
module "ecs_vpc" {
  source         = "../modules/aws/vpc"
  vpc_name       = var.cluster_name
  region_name    = var.region_name
  vpc_cidr_block = var.vpc_cidr_block
}

module "ecs_cluster" {
  source         = "../modules/aws/ecs"
  region_name    = var.region_name
  cluster_name   = var.cluster_name
  instance_type  = var.instance_type
  instance_count = var.instance_count
  vpc_id         = module.ecs_vpc.vpc.id
  subnet_ids     = module.ecs_vpc.subnet_ids
}
```
I left a working example in `terraform/ecs-eu-central-1` folder. This sample environment is parametrized from `tfvars` file. 
```
region_name       = "eu-central-1"
cluster_name      = "ecs-dev-euc1"
instance_type     = "t3.micro"
instance_count    = 3
vpc_cidr_block    = "172.18.0.0/16"
create_repository = true
repository_name   = "django-blog"
```
You could notice a `create_repository` and `repository_name` parameters. They are is used to create an ECR repository. In real life repository and cluster definitions should not be mixed, but I added it right here for the sake of brevity. Terraform plan can be produced by output of the following command: 
```bash
terraform plan -var-file=env.tfvars 
```

### Docker Compose

Previously I used to be involved more either in Kubernetes projects (including on-prem, AKS and EKS) or on-prem docker compose solutions. And although I am quite experienced Docker user this was my first time when I encountered ECS. So not everything went smoothly but I did my best. 

I put Docker file and `docker-compose.yml` into django-blog folder. There is also an build.sh script to simulate build pipeline. To run the solution you first neet to provision ECS and ECR as was described in the previous section. Next you will need to setup an docker context:
```bash
docker context create ecs ecs-dev-euc1 # the latter is cluster name 
```
After that you can launch the app:
```bash
$ docker compose up
[+] Running 10/12
[+] Running 10/12            CreateInProgress User Initiated                                                                                                                                   97.8s
 ⠸ django-blog               CreateInProgress User Initiated
 ...
 ```

Unfortunately I didn't manage to make the application externally accessible due to constraints in *django_diy_blog/settings.py*:
```python
ALLOWED_HOSTS = ['.herokuapp.com','127.0.0.1']
```
Besides that app setup require creating a superuser and thsi step relies on console input. Fixing setting and automating superuser creation was going beyond of exercise scope. In real projects though this would need to be addressed. 
